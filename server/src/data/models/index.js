import associate from '../db/associations';

import User from './user';
import Message from './message';
import MessageReaction from './messageReaction';

associate({
  User,
  Message,
  MessageReaction
});

export {
  User as UserModel,
  Message as MessageModel,
  MessageReaction as MessageReactionModel
};
