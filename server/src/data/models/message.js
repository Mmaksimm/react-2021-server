import { DataTypes } from 'sequelize';
import orm from '../db/connection';

export default orm.define('message', {
  text: {
    allowNull: false,
    type: DataTypes.TEXT
  },
  createdAt: DataTypes.DATE,
  updatedAt: DataTypes.DATE
});
