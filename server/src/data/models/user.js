import { DataTypes } from 'sequelize';
import orm from '../db/connection';

export default orm.define('user', {
  login: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true
  },
  password: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: false
  },
  avatar: {
    allowNull: true,
    type: DataTypes.STRING,
    unique: false
  },
  isAdmin: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false
  },
  createdAt: DataTypes.DATE,
  updatedAt: DataTypes.DATE
});
