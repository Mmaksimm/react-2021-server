import { DataTypes } from 'sequelize';
import orm from '../db/connection';

export default orm.define('messageReaction', {
  isLike: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  createdAt: DataTypes.DATE,
  updatedAt: DataTypes.DATE
});
