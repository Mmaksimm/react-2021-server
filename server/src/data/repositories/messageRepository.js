import sequelize from '../db/connection';
import { MessageModel, UserModel, MessageReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "messageReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class MessageRepository extends BaseRepository {
  getMessages() {
    return this.model.findAll({
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'login', 'avatar']
      }, {
        model: MessageReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'message.id',
        'user.id'
      ],
      order: [['createdAt', 'DESC']]
    });
  }

  getMessageById(id) {
    return this.model.findOne({
      group: [
        'message.id',
        'user.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'login', 'avatar']
      }, {
        model: MessageReactionModel,
        attributes: []
      }]
    });
  }
}

export default new MessageRepository(MessageModel);
