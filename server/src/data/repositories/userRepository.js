import { UserModel } from '../models/index';
import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  addUser(user) {
    return this.create(user);
  }

  getByLogin(login) {
    return this.model.findOne({ where: { login } });
  }

  getUsers() {
    return this.model.findAll({
      attributes: ['id', 'login', 'avatar', 'isAdmin']
    });
  }

  getUserById(id) {
    return this.model.findOne({
      where: { id },
      attributes: ['id', 'login', 'avatar', 'isAdmin']
    });
  }
}

export default new UserRepository(UserModel);
