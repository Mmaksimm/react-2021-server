/* eslint-disable no-console */
import usersSeed from '../seed-data/usersSeed';
import messagesSeed from '../seed-data/messagesSeed';

export default {
  up: async (queryInterface, Sequelize) => {
    try {
      const options = {
        type: Sequelize.QueryTypes.SELECT
      };

      // Add users.
      const usersMappedSeed = usersSeed.map(user => ({
        ...user
      }));
      await queryInterface.bulkInsert('users', usersMappedSeed, {});
      const users = await queryInterface.sequelize.query('SELECT id, login FROM "users";', options);

      // Add messages.
      const messagesMappedSeed = messagesSeed.map(({ login: userLogin, ...message }) => {
        const { id } = users.find(({ login }) => login === userLogin);
        return {
          ...message,
          userId: id
        };
      });
      await queryInterface.bulkInsert('messages', messagesMappedSeed, {});
    } catch (err) {
      console.log(`Seeding error: ${err}`);
    }
  },
  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('messages', null, {});
      await queryInterface.bulkDelete('users', null, {});
    } catch (err) {
      console.log(`Seeding error: ${err}`);
    }
  }
};
