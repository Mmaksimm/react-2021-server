export default models => {
  const {
    User,
    Message,
    MessageReaction
  } = models;

  User.hasMany(Message);
  Message.belongsTo(User);

  Message.hasMany(MessageReaction);
  MessageReaction.belongsTo(Message);

  User.hasMany(MessageReaction);
  MessageReaction.belongsTo(User);
};
