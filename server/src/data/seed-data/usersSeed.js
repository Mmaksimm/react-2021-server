import { encryptSync } from '../../helpers/cryptoHelper';

const hash = password => encryptSync(password);
const now = new Date();

const userSeed = [{
  login: 'admin',
  password: hash('admin'),
  isAdmin: true
}, {
  login: 'Ruth',
  password: hash('Ruth'),
  avatar: (
    'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA'
  ),
  isAdmin: false
}, {
  login: 'Wendy',
  password: hash('Wendy'),
  avatar: (
    'https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng'
  ),
  isAdmin: false
}, {
  login: 'Helen',
  password: hash('Helen'),
  avatar: (
    'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ'
  ),
  isAdmin: false
}, {
  login: 'Ben',
  password: hash('Ben'),
  avatar: 'https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg',
  isAdmin: false
}].map(user => ({
  ...user,
  createdAt: now,
  updatedAt: now
}));

export default userSeed;
