import fs from 'fs';
import express from 'express';
import cors from 'cors';
// import path from 'path';
import passport from 'passport';
import http from 'http';
import socketIO from 'socket.io';
// import helmet from 'helmet';
// import morgan from 'morgan';
import routes from './api/routes/index';
import authorizationMiddleware from './api/middlewares/authorizationMiddleware';
import errorHandlerMiddleware from './api/middlewares/errorHandlerMiddleware';
import routesWhiteList from './config/routesWhiteListConfig';
import socketInjector from './socket/injector';
import socketHandlers from './socket/handlers';
import sequelize from './data/db/connection';
import './config/passportConfig';
// import logger, { logStream } from './config/loggerConfig';

// import { host, port } from './config/config';

const app = express();
app.use(cors());
const socketServer = http.Server(app);
const io = socketIO(socketServer);

sequelize
  .authenticate()
  .then(() => {
    // logger.info('Connected to the database.');
  })
  .catch(() => {
    // logger.error('Could not connect to the database.', error);
  });

io.on('connection', socketHandlers);

// app.use(helmet());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
// app.use(morgan('combined', { stream: logStream }));

app.use(passport.initialize());

app.use(socketInjector(io));

app.use('/api/', authorizationMiddleware(routesWhiteList));

routes(app, io);

app.use((req, res, next) => {
  res.set({
    'Cache-Control': [
      'max-age=34164000',
      'public'
    ]
  });
  next();
});

app.use(express.static('./dist/client'));

app.get('*', (req, res) => {
  res.write(fs.readFileSync('./dist/client/index.html'));
  res.end();
});

app.use(errorHandlerMiddleware);

app.listen(process.env.PORT || 3001, () => {
  // logger.info(`Server started at ${host}:${port}`);
});

// socketServer.listen(process.env.PORT);
