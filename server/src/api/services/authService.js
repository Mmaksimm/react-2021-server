import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';

export const login = async ({ id }) => {
  const { login: name, avatar, isAdmin } = await userRepository.getUserById(id);
  return {
    user: {
      id,
      login: name,
      avatar,
      isAdmin
    },
    token: createToken({ id })
  };
};

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};
