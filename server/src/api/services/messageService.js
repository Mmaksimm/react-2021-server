/* eslint-disable no-console */
/* eslint-disable no-nested-ternary */
import messageRepository from '../../data/repositories/messageRepository';
import messageReactionRepository from '../../data/repositories/messageReactionRepository';

export const getMessages = () => messageRepository.getMessages();

export const getMessageById = id => messageRepository.getMessageById(id);

export const create = async (userId, message) => {
  const { id } = await messageRepository.create({
    ...message,
    userId
  });
  const newMessage = await getMessageById(id);
  const { user: { login } } = newMessage;
  return {
    login,
    newMessage,
    messageId: id
  };
};

export const updateMessage = async (messageId, body) => {
  await messageRepository.updateById(
    messageId,
    body
  );
  const { userId, updatedAt, user: { login } } = await getMessageById(messageId);
  return {
    userId,
    messageId,
    login,
    updatedAt
  };
};

export const deleteMessage = async messageId => {
  const {
    user: {
      id: userId,
      login
    }
  } = await getMessageById(messageId);

  await messageRepository.deleteById(messageId);

  return {
    messageId,
    userId,
    login
  };
};

export const setReaction = async (userId, { messageId }) => {
  const reaction = await messageReactionRepository.getMessageReaction(userId, messageId);
  const hasReaction = Boolean(reaction);

  if (hasReaction) {
    await messageReactionRepository.deleteById(reaction.id);
  } else {
    await messageReactionRepository.create({ userId, messageId, isLike: true });
  }

  const diffLike = hasReaction
    ? -1
    : 1;

  return { diffLike };
};
