import userRepository from '../../data/repositories/userRepository';

export const getUsers = () => userRepository.getUsers();

export const getUserById = async userId => userRepository.getUserById(userId);

export const create = user => userRepository.create(user);

export const updateUser = async (userId, body) => {
  await userRepository.updateById(
    userId,
    body
  );
  const { updatedAt } = await getUserById(userId);
  return {
    updatedAt
  };
};

export const deleteUser = async userId => {
  await userRepository.deleteById(userId);
};
