import { Router } from 'express';
import * as messageService from '../services/messageService';

const router = Router();

router
  .get('/', (req, res, next) => messageService.getMessages()
    // eslint-disable-next-line no-console
    .then(messages => res.send(messages))
    .catch(next))
  .get('/:id', (req, res, next) => messageService.getMessageById(req.params.id)
    .then(message => res.send(message))
    .catch(next))
  .post('/', (req, res, next) => messageService.create(req.user.id, req.body)
    .then(({ messageId, login, newMessage }) => {
      req.io.emit('new_message', { login, messageId, userId: req.user.id });
      return res.send(newMessage);
    })
    .catch(next))
  .put('/react', (req, res, next) => messageService.setReaction(req.user.id, req.body)
    .then(({ diffLike }) => {
      const { messageId } = req.body;
      req.io.emit('update_message', { messageId, userId: req.user.id });
      if ((req.body.user !== req.user.id) && (diffLike === 1 || diffLike === 1)) {
        req.io.to(req.body.user)
          .emit('like', {
            messageId,
            message: 'Your message was liked!'
          });
      }
      return res.send({ diffLike });
    })
    .catch(next))
  .put('/:id', (req, res, next) => messageService.updateMessage(req.params.id, req.body)
    .then(({ updatedAt, ...message }) => {
      req.io.emit('update_message', message);
      res.send({ updatedAt });
    })
    .catch(next))
  .delete('/:id', (req, res, next) => messageService.deleteMessage(req.params.id)
    .then(response => {
      req.io.emit('delete_message', response);
      res.send({ success: true });
    })
    .catch(next));

export default router;
