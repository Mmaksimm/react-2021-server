import adminMiddleware from '../middlewares/adminMiddleware';
import authRoutes from './authRoutes';
import messageRoutes from './messageRoutes';
import userRoutes from './userRoutes';

// register all routes
export default app => {
  app.use('/api/auth', authRoutes);
  app.use('/api/messages', messageRoutes); // messages
  app.use('/api/users', adminMiddleware, userRoutes);
};
