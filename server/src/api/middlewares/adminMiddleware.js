const adminMiddleware = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    throw Error('No access rights');
  }
};

export default adminMiddleware;
