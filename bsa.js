import Chat from './src/containers/Chat';
import rootReducer from './src/containers/Chat/reducer';

export default { Chat, rootReducer };