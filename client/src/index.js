// eslint-disable-next-line no-unused-vars
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import store from './store';
import { Provider } from 'react-redux';
import Routing from './common/containers/Routing';

import './styles/index.css'
import 'semantic-ui-css/semantic.min.css';

// const url = 'https://edikdolynskyi.github.io/react_sources/messages.json';
const root = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Routing />
    </Router>
  </Provider>
  , root);
