import callWebApi from '../helpers/webApiHelper';
import ApiPath from '../common/constants/enums/ApiPath';

const { Chat } = ApiPath;

export const getAllMessages = async () => {
  const response = await callWebApi({
    endpoint: Chat,
    type: 'GET'
  });
  return response.json();
};

export const getMessage = async id => {
  const response = await callWebApi({
    endpoint: `${Chat}/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const addMessage = async request => {
  const response = await callWebApi({
    endpoint: Chat,
    type: 'POST',
    request
  });
  return response.json();
};

export const updateMessage = async ({ messageId, ...updatedMessage }) => {
  const response = await callWebApi({
    endpoint: `${Chat}/${messageId}`,
    type: 'PUT',
    request: updatedMessage
  });
  return response.json();
};

export const deleteMessage = async messageId => {
  const response = await callWebApi({
    endpoint: `${Chat}/${messageId}`,
    type: 'DELETE'
  });
  return response.json();
};

export const likeMessage = async (messageId) => {
  const response = await callWebApi({
    endpoint: `${Chat}/react`,
    type: 'PUT',
    request: {
      messageId
    }
  });
  return response.json();
};
