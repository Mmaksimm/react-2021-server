import callWebApi from '../helpers/webApiHelper';
import ApiPath from '../common/constants/enums/ApiPath';

const { Users } = ApiPath;

export const getAllUsers = async () => {
  const response = await callWebApi({
    endpoint: Users,
    type: 'GET'
  });
  return response.json();
};

export const getUser = async id => {
  const response = await callWebApi({
    endpoint: `${Users}/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const addUser = async request => {
  const response = await callWebApi({
    endpoint: Users,
    type: 'POST',
    request
  });
  return response.json();
};

export const updateUser = async ({ userId, ...updatedUser }) => {
  const response = await callWebApi({
    endpoint: `${Users}/${userId}`,
    type: 'PUT',
    request: updatedUser
  });
  return response.json();
};

export const deleteUser = async userId => {
  const response = await callWebApi({
    endpoint: `${Users}/${userId}`,
    type: 'DELETE'
  });
  return response.json();
};

