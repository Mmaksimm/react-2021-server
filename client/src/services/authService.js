import callWebApi from '../helpers/webApiHelper';
import ApiPath from '../common/constants/enums/ApiPath';

const { Auth } = ApiPath;

export const login = async request => {
  const response = await callWebApi({
    endpoint: `${Auth}login`,
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: `${Auth}register`,
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: `${Auth}user`,
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};
