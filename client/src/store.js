import { configureStore } from '@reduxjs/toolkit';

import * as messageService from './services/messageService';
import * as authService from './services/authService';
import * as userService from './services/userService';

import chatReducer from './common/reducers/chatReducer';
import profileReducer from './common/reducers/profileReducer';
import usersReducer from './common/reducers/usersReducer';

const store = configureStore({
  reducer: {
    chat: chatReducer,
    profile: profileReducer,
    users: usersReducer
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      thunk: {
        extraArgument: {
          messageService,
          authService,
          userService
        }
      }
    });
  },
});

export default store;
