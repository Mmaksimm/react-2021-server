/* eslint-disable react/jsx-no-duplicate-props */
//* eslint-disable no-useless-constructor */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Container, Form, Button } from 'semantic-ui-react';
import { addNewMessage } from '../../../../common/actions/chatActions';

import styles from './styles.module.scss';

const MessageInput = () => {
  const dispatch = useDispatch();

  const [getText, setText] = useState('')

  const HandlerOnChange = ({ target: { value } }) => {
    setText(value);
  }

  const handlerAddNewMessage = (event) => {
    event.preventDefault();
    const text = getText.trim();
    if (!text) return
    dispatch(addNewMessage(text));
    setText('');
  }

  return (
    <Container className={`${styles.newMessage} message-input`}>
      <Form onSubmit={handlerAddNewMessage} className={styles.form}>
        <
          input
          className={`${styles.input} message-input-text`}
          placeholder="New Message"
          value={getText}
          onChange={HandlerOnChange}
        />
        <
          Button
          className={`${styles.send} message-input-button`}
          type="submit"
        >
          Send
        </Button>
      </Form>
    </Container>
  )
}

export default MessageInput;
