import React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { Label, Icon, Comment as MessageUI } from 'semantic-ui-react';
import { getUserImgLink } from '../../../../helpers/imageHelper';
import getDateForMessage from '../../../../helpers/getDateForMessage'
import { likeToggle } from '../../../../common/actions/chatActions';

import styles from './styles.module.scss';

const Message = ({ message }) => {
  const dispatch = useDispatch();
  const {
    id,
    text,
    createdAt,
    likeCount,
    user: {
      avatar,
      login
    }
  } = message

  const likeToggleHandler = (event) => {
    event.preventDefault();
    dispatch(likeToggle({ likeCount, messageId: id }));
  }
  const dateForMessage = getDateForMessage(createdAt);

  return (
    <div className={`${styles.separate} message`}>
      <MessageUI.Group className={styles.message}>
        <MessageUI>
          <MessageUI.Avatar
            src={getUserImgLink(avatar)}
            className={`${styles.avatar} message-user-avatar`}
          />
          <MessageUI.Content>
            <MessageUI.Author as="a" className="message-user-name">
              {login}
            </MessageUI.Author>
            <MessageUI.Metadata className="message-time">
              {dateForMessage}
            </MessageUI.Metadata>
            <MessageUI.Text className="message-text" >
              {text}
            </MessageUI.Text>
          </MessageUI.Content>
          <div className={styles.messageAction}>
            <Label
              basic
              size="small"
              as="a"
              onClick={likeToggleHandler}
              className={styles.toolbarBtn}
            >
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          </div>
        </MessageUI>
      </MessageUI.Group>
    </div>
  );
}

Message.propTypes = {
  message: PropTypes.objectOf(
    PropTypes.oneOfType(
      [
        PropTypes.string,
        PropTypes.bool,
        PropTypes.objectOf(
          PropTypes.string
        )
      ]
    )
  ).isRequired
};

export default Message;
