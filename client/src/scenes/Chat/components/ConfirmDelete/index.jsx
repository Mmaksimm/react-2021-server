import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Confirm } from 'semantic-ui-react';
import { useHistory } from "react-router-dom";
import RoutePath from '../../../../common/constants/enums/RoutePath';

import { cancel, deleteMessage } from '../../../../common/actions/chatActions';

const ConfirmDelete = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const {
    modalDeleteMessageId,
  } = useSelector(({ chat }) => chat);

  const handlerDelete = () => {
    dispatch(deleteMessage(modalDeleteMessageId));
    history.push(RoutePath.Chat);
  };

  const handlerCancel = () => {
    dispatch(cancel());
    history.push(RoutePath.Chat);
  }

  return (
    <Confirm
      open
      content='Do you want to delete the message?'
      onCancel={handlerCancel}
      onConfirm={handlerDelete}
    />
  );
};

export default ConfirmDelete;
