import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import { Button, Icon, Modal, Form } from 'semantic-ui-react';
import RoutePath from '../../../../common/constants/enums/RoutePath';

import { cancel, updateMessage } from '../../../../common/actions/chatActions';

import styles from './styles.module.scss';

const EditMessage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const {
    modalEditMessage: message,
    editModal
  } = useSelector(({ chat }) => chat);

  const [getText, setText] = useState(message.text)
  const HandlerOnChange = ({ target: { value } }) => {
    setText(value);
  }

  const handlerUpdate = async () => {
    const text = getText.trim();
    if (!text || getText === message.text) return;
    await dispatch(updateMessage({ text: getText, messageId: message.id }))
    history.push(RoutePath.Chat);
  }

  const handlerCancel = () => {
    dispatch(cancel());
    history.push(RoutePath.Chat);
  }

  return (
    <Modal open={editModal} className={`edit-message-modal ${editModal ? 'modal-shown' : ''}`}>
      <Modal.Header className={styles.modalHeader}>Edit your message</Modal.Header>
      <Modal.Content className={styles.modalContent}>
        <Modal.Description>
          <Form >
            <div>
              <
                textarea
                name="body"
                placeholder="What is the news?"
                rows="3"
                className="edit-message-input"
                value={getText}
                onChange={HandlerOnChange}
              />
            </div>
          </Form>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions className={styles.modalAction}>
        <Button.Group>
          <Button icon onClick={handlerUpdate} className="edit-message-button">
            <Icon name="save" />
          </Button>
          <Button icon onClick={handlerCancel} className="edit-message-close">
            <Icon name="window close" />
          </Button>
        </Button.Group>
      </Modal.Actions>
    </Modal>
  )
}

export default EditMessage
