/* eslint-disable no-useless-constructor */
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';
import { Button, Icon, Label, Comment as MessageUI } from 'semantic-ui-react';
import getDateForMessage from '../../../../helpers/getDateForMessage';
import { confirmDeleteMessage, editMessage } from '../../../../common/actions/chatActions';
import RoutePath from '../../../../common/constants/enums/RoutePath';


import styles from './styles.module.scss';

const OwnMessage = ({
  message
}) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const {
    id,
    text,
    createdAt,
    likeCount,
  } = message

  const dateForMessage = getDateForMessage(createdAt);

  const handlerEditMessage = () => {
    dispatch(editMessage(message));
    history.push(RoutePath.ChatEdit);
  };

  const handlerConfirmDeleteMessage = () => {
    dispatch(confirmDeleteMessage(id));
    history.push(RoutePath.ChatConfirmDelete);
  }

  return (
    <div className={`${styles.separate} own-message`} >
      <MessageUI.Group className={styles.myMessage}>
        <MessageUI>
          <MessageUI.Content>
            <MessageUI.Metadata className="message-time">
              {dateForMessage}
            </MessageUI.Metadata>
            <MessageUI.Text className={`${styles.text} message-text`}>
              {text}
            </MessageUI.Text>
          </MessageUI.Content>
          <div className={styles.messageAction}>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
            >
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
            <Button.Group floated="right" size="mini" className={styles.actionMessage}>
              <Button icon onClick={handlerEditMessage} className="message-edit">
                <Icon name="cog" />
              </Button>
              <Button icon onClick={handlerConfirmDeleteMessage} className="message-delete">
                <Icon name="trash" />
              </Button>)
            </Button.Group>
          </div>
        </MessageUI>
      </MessageUI.Group>
    </div>
  );
}

OwnMessage.propTypes = {
  message: PropTypes.objectOf(
    PropTypes.oneOfType(
      [
        PropTypes.string,
        PropTypes.bool,
        PropTypes.objectOf(
          PropTypes.string
        )
      ]
    )
  ).isRequired
};

export default OwnMessage;
