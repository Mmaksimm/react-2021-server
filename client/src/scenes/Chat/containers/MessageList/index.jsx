/* eslint-disable no-undef */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'semantic-ui-react';
import getStartDate from '../../../../helpers/getStartDate';
import Preloader from '../../../../common/components/Preloader';
import Message from '../../components/Message';
import OwnMessage from '../../components/OwnMessage';
import Divider from '../../components/Divider';

import styles from './styles.module.scss';

const MessageList = ({
  messages,
  preloader,
  ownUserId
}) => {
  let date = null;
  const secondsOfDay = 86400000;
  const startNewDate = ({ createdAt }) => {
    if (!date || (getStartDate(createdAt) - getStartDate(date)) > secondsOfDay) {
      date = new Date(createdAt);
      return true;
    }
    return false
  };

  return (
    <Card className={`${styles.card} message-list`}>
      {
        !preloader
          ? (
            messages.length && messages
              .map(message => message)
              .sort((messagePrev, message) => (Date.parse(messagePrev?.createdAt) - Date.parse(message?.createdAt)))
              .map(message => {
                return <Fragment key={message.id}>
                  {startNewDate(message) && <Divider date={message.createdAt} />}
                  {ownUserId !== message.user.id
                    ? (
                      <Message
                        message={message}
                      />
                    )
                    : (
                      <OwnMessage
                        key={message.id}
                        message={message}
                      />
                    )
                  }
                </Fragment>
              })
          )
          : <Preloader />
      }
    </Card>
  )
}

MessageList.propTypes = {
  messages: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType(
        [
          PropTypes.string,
          PropTypes.bool,
          PropTypes.objectOf(
            PropTypes.string
          )
        ]
      )
    )
  ),
  ownUserId: PropTypes.string,
  preloader: PropTypes.bool.isRequired
};

MessageList.defaultProps = {
  messages: [{ id: '' }],
  ownUserId: ''
};

export default MessageList;
