/* eslint-disable no-undef */
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container } from 'semantic-ui-react';
import MessageList from '../MessageList'
import Header from '../../components/Header';
import MessageInput from '../../components/MessageInput';

import styles from './styles.module.scss';

import { setMessages } from '../../../../common/actions/chatActions';

const Chat = () => {
  const {
    messages,
    preloader
  } = useSelector(({ chat }) => chat);

  const ownUserId = useSelector(({ profile: { user: { id } } }) => id);

  const dispatch = useDispatch();
  useEffect(() => {
    if (!preloader && !messages.length) dispatch(setMessages());
  });

  return (
    <Container className={`${styles.chart} chart`}>
      <Header messages={messages} />
      <MessageList
        messages={messages}
        preloader={preloader}
        ownUserId={ownUserId}
      />
      <MessageInput />
    </Container>
  )
};

export default Chat;
