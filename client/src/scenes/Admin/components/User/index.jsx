/* eslint-disable no-undef */
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { List, Image, Button } from 'semantic-ui-react';
import { editUserAction, confirmDeleteUser } from '../../../../common/actions/userActions';
import RoutePath from '../../../../common/constants/enums/RoutePath';

import styles from './styles.module.scss';


const User = ({ user }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const {
    id,
    login,
    avatar
  } = user
  const handleEditUser = async () => {
    await dispatch(editUserAction({
      newUser: false,
      edit: true,
      editUser: { ...user },
    }));

    history.push(RoutePath.AdminEditUser);
  };

  const handleConfirmDeleteUser = async () => {
    await dispatch(confirmDeleteUser(id));
    history.push(RoutePath.AdminConfirmDeleteUser);
  }

  return (
    <List.Item className={styles.user} >
      <div className={styles.content}>
        <div className={styles.avatar}>
          <Image avatar src={avatar} />
        </div>
        <List.Content className={styles.data}>
          <List.Header className={styles.name} >{login}</List.Header>
        </List.Content>
      </div>
      <div className={styles.actions}>
        <Button className={styles.action} onClick={handleEditUser}>Edit</Button>
        <Button className={styles.action} onClick={handleConfirmDeleteUser}>Delete</Button>
      </div>
    </List.Item>
  )
}

export default User;
