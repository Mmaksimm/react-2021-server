import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import RoutePath from '../../../../common/constants/enums/RoutePath';
import { addNewUser, updateUser, editUserAction } from '../../../../common/actions/userActions';

import { Form, Button, Segment } from 'semantic-ui-react';

const EditUserForm = () => {
  const {
    newUser,
    editUser
  } = useSelector(({ users }) => users);
  const dispatch = useDispatch();
  const history = useHistory();
  const [getLogin, setLogin] = useState(newUser ? '' : editUser.login);
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const loginChanged = data => {
    setLogin(data);
  };

  const passwordChanged = data => {
    setPassword(data);
  };

  const handleCancel = () => {
    history.push(RoutePath.AdminListUsers);
    dispatch(editUserAction({
      newUser: false,
      edit: false,
      editUser: {},
    }));
  }

  const handleUpdate = async () => {
    setIsLoading(true);
    try {
      if (newUser) {
        await dispatch(addNewUser({ password, login: getLogin }));
      } else {
        await dispatch(updateUser({ userId: editUser.id, password, login: getLogin }));
      }
      await handleCancel();
    } catch {
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={ev => ev.preventDefault()}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Loin"
          type="text"
          value={getLogin}
          onChange={ev => loginChanged(ev.target.value)}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          value={password}
          onChange={ev => passwordChanged(ev.target.value)}
        />
        <Button fluid size="large" loading={isLoading} primary onClick={handleCancel}>
          Cancel
        </Button>
        <br />
        <Button color="teal" fluid size="large" primary onClick={handleUpdate}>
          {newUser ? 'Create' : 'Update'}
        </Button>
      </Segment>
    </Form>
  );
};

export default EditUserForm;
