import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Confirm } from 'semantic-ui-react';
import { useHistory } from "react-router-dom";
import RoutePath from '../../../../common/constants/enums/RoutePath';

import { confirmDeleteUser, deleteUser } from '../../../../common/actions/userActions';

const ConfirmDelete = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const {
    modalDeleteUserId = undefined
  } = useSelector(({ users }) => users);

  const handleCancel = () => {
    history.push(RoutePath.AdminListUsers);
    dispatch(confirmDeleteUser(''));
  }

  const handlerDelete = async () => {
    await dispatch(deleteUser(modalDeleteUserId));
    handleCancel();
  };

  return (
    <Confirm
      open
      content='Do you want to delete the user?'
      onCancel={handleCancel}
      onConfirm={handlerDelete}
    />
  );
};

export default ConfirmDelete;
