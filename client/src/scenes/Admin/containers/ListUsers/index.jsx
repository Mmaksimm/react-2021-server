/* eslint-disable no-undef */
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { List, Container, Button } from 'semantic-ui-react';
import { getUsers, editUserAction } from '../../../../common/actions/userActions';
import User from '../../components/User';
import Preloader from '../../../../common/components/Preloader';
import RoutePath from '../../../../common/constants/enums/RoutePath';

import styles from './styles.module.scss';

const ListUsers = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const {
    users,
    preloader
  } = useSelector(({ users }) => users);

  useEffect(() => {
    if (!users.length && !preloader) dispatch(getUsers());
  }, [users?.length, preloader, dispatch]);

  const handleAddUser = async () => {
    await dispatch(editUserAction({
      newUser: true,
      edit: true,
      editUser: {},
    }));

    history.push(RoutePath.AdminEditUser);
  };

  return (!preloader
    ? (users?.length && (
      <Container className={styles.container}>
        <List divided verticalAlign='middle' className={styles.users} size="massive">
          {users.map(user => <User user={user} key={user.id} />)}
        </List>
        <Button size="massive" fluid onClick={handleAddUser} >Add User</Button>
      </Container>)
    )
    : <Preloader />
  )
};

export default ListUsers;
