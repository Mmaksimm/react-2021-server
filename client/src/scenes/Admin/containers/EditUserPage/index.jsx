import React from 'react';
import { useSelector } from 'react-redux';
// import { useHistory } from 'react-router-dom';
import { Grid, Header } from 'semantic-ui-react';
import EditUserForm from '../../components/EditUserForm';
// import RoutePath from '../../../../common/constants/enums/RoutePath';

import styles from './styles.module.scss'

const EditUserPage = () => {
  const {
    //   edit,
    newUser
  } = useSelector(({ users }) => users);

  // const history = useHistory()
  // if (!edit) history.push(RoutePath.Login)

  return (
    <Grid textAlign="center" verticalAlign="middle" className={styles.form}>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="teal" textAlign="center">
          {newUser
            ? 'Add new user'
            : 'Edit user'
          }
        </Header>
        <EditUserForm />
      </Grid.Column>
    </Grid>
  );
};

export default EditUserPage;
