import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import RoutePath from '../../../../common/constants/enums/RoutePath';
import { login } from '../../../../common/actions/profileActions';
import { Form, Button, Segment } from 'semantic-ui-react';

const LoginForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [getLogin, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const loginChanged = data => {
    setLogin(data);
  };

  const passwordChanged = data => {
    setPassword(data);
  };

  const handleLoginClick = async () => {
    setIsLoading(true);
    try {
      await dispatch(login({ password, login: getLogin }));
      history.push(RoutePath.Chat);
    } catch (err) {
      // TODO: show error
      // showError('Application login failed.');
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Loin"
          type="text"
          onChange={ev => loginChanged(ev.target.value)}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          onChange={ev => passwordChanged(ev.target.value)}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Login
        </Button>
      </Segment>
    </Form>
  );
};

export default LoginForm;
