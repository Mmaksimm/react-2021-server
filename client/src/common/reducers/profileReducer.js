import { createReducer } from '@reduxjs/toolkit';
// import RoutePath from '../constants/enums/RoutePath';

import {
  login
} from '../actions/profileActions';

const initialState = {
  isLoading: false,
  isOnce: false
};

export const profileReducer = createReducer(initialState, (builder) => {
  builder.addCase(login.pending, (state) => {
    state.isAuthorized = false;
    state.isLoading = true;
    state.isOnce = true;
  });

  builder.addCase(login.rejected, (state) => {
    state.isLoading = false;
  });

  builder.addCase(login.fulfilled, (state, { payload: { user } }) => {
    state.user = user;
    state.isAuthorized = Boolean(user?.id);
    state.isLoading = false;
  });
})

export default profileReducer;
