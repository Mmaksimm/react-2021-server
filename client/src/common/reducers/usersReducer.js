/* eslint-disable import/no-anonymous-default-export */
import { createReducer } from '@reduxjs/toolkit';

import {
  getUsers,
  addNewUser,
  editUserAction,
  cancel,
  confirmDeleteUser,
  deleteUser,
  updateUser
} from '../actions/userActions';

const initialState = {
  users: [],
  edit: false,
  newUser: false,
  preloader: false,
  editUser: {},
  modalDeleteUserId: ''
};

const usersReducer = createReducer(initialState, (builder) => {
  builder.addCase(getUsers.pending, (state) => {
    state.preloader = true;
  });

  builder.addCase(getUsers.fulfilled, (state, { payload: { users } }) => {
    state.users = users;
    state.preloader = false;
  });

  builder.addCase(getUsers.rejected, (state, { payload: { users } }) => {
    state.preloader = false;
  });

  builder.addCase(addNewUser.fulfilled, (state, { payload: { user } }) => {
    state.users = [...state.users, user];
  });

  builder.addCase(editUserAction, (state, { payload: { newUser, edit, editUser } }) => {
    state.newUser = newUser;
    state.edit = edit;
    state.editUser = editUser;
  });

  builder.addCase(cancel, (state) => {
    state.modalEditUser = {};
    state.modalDeleteUserId = {};
    state.editModal = false;
  });

  builder.addCase(confirmDeleteUser, (state, { payload }) => {
    state.modalDeleteUserId = payload;
  });

  builder.addCase(deleteUser.fulfilled, (state) => {
    state.users = state.users.filter(({ id }) => id !== state.modalDeleteUserId)
    state.modalDeleteUserId = {};
  });

  builder.addCase(updateUser.fulfilled, (state, { payload: { userId, update } }) => {
    state.users = state.users.map(user => (
      user.id !== userId
        ? user
        : {
          ...user,
          ...update
        }

    ));
    state.modalEditUser = {};
    state.editModal = false;
  });
})

export default usersReducer;
