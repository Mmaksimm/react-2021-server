/* eslint-disable import/no-anonymous-default-export */
import { createReducer } from '@reduxjs/toolkit';

import {
  setMessages,
  addNewMessage,
  editMessage,
  cancel,
  confirmDeleteMessage,
  deleteMessage,
  updateMessage
} from '../actions/chatActions';

const initialState = {
  messages: [],
  editModal: false,
  preloader: false,
  modalEditMessage: {},
  modalDeleteMessageId: '',
  profile: {}
};

export const chatReducer = createReducer(initialState, (builder) => {
  builder.addCase(setMessages.pending, (state) => {
    state.preloader = true;
  });

  builder.addCase(setMessages.fulfilled, (state, { payload: { profile, messages } }) => {
    state.messages = messages;
    state.preloader = false;
    state.profile = profile;
  });

  builder.addCase(addNewMessage.fulfilled, (state, { payload: { message } }) => {
    state.messages = [...state.messages, message];
  });

  builder.addCase(editMessage, (state, { payload }) => {
    state.modalEditMessage = payload;
    state.editModal = true;
  });

  builder.addCase(cancel, (state) => {
    state.modalEditMessage = {};
    state.modalDeleteMessageId = {};
    state.editModal = false;
  });

  builder.addCase(confirmDeleteMessage, (state, { payload }) => {
    state.modalDeleteMessageId = payload;
  });

  builder.addCase(deleteMessage.fulfilled, (state) => {
    state.messages = state.messages.filter(({ id }) => id !== state.modalDeleteMessageId)
    state.modalDeleteMessageId = {};
  });

  builder.addCase(updateMessage.fulfilled, (state, { payload: { messageId, update } }) => {
    state.messages = state.messages.map(message => (
      message.id !== messageId
        ? message
        : {
          ...message,
          ...update
        }

    ));
    state.modalEditMessage = {};
    state.editModal = false;
  });
})

export default chatReducer;
