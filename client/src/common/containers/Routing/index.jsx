import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Route, Switch, useHistory } from 'react-router-dom';
import PrivateRoute from '../PrivateRoute';
import PublicRoute from '../PublicRoute';
import AdminRoute from '../AdminRoute';
import RoutePath from '../../../common/constants/enums/RoutePath';
import Chat from '../../../scenes/Chat/containers/Chat';
import EditMessage from '../../../scenes/Chat/components/EditMessage';
import ConfirmDelete from '../../../scenes/Chat/components/ConfirmDelete';
import ListUsers from '../../../scenes/Admin/containers/ListUsers';
import Login from '../../../scenes/Login/containers/LoginPage';
import Profile from '../../components/Profile';
import Preloader from '../../components/Preloader';
import NotFound from '../../../scenes/NotFound';
import EditUserPage from '../../../scenes/Admin/containers/EditUserPage';
import ConfirmDeleteUser from '../../../scenes/Admin/components/ConfirmDeleteUser';
import { loadCurrentUser } from '../../../common/actions/profileActions';

const Routing = () => {
  const {
    user,
    isAuthorized,
    isLoading,
    isOnce
  } = useSelector(({ profile }) => profile);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (!isAuthorized && !isLoading && !isOnce) {
      dispatch(loadCurrentUser());
    }

    if (!isAuthorized && !isLoading && isOnce) {
      history.push(RoutePath.Login)
    }

    if (isAuthorized && !isLoading && isOnce) {
      history.push(RoutePath.Chat)
    }

  }, [isAuthorized, isLoading, isOnce, dispatch, history]);

  return (
    isLoading
      ? <Preloader />
      : (
        <>
          {isAuthorized && (
            <header>
              <Profile user={user} />
            </header>
          )}
          <main>
            <Switch>
              <PublicRoute exact path={RoutePath.Login} component={Login} />
              <PrivateRoute exact path={RoutePath.Chat} component={Chat} />
              <PrivateRoute exact path={RoutePath.ChatEdit} component={EditMessage} />
              <PrivateRoute exact path={RoutePath.ChatConfirmDelete} component={ConfirmDelete} />
              <AdminRoute exact path={RoutePath.AdminListUsers} component={ListUsers} />
              <AdminRoute exact path={RoutePath.AdminEditUser} component={EditUserPage} />
              <AdminRoute exact path={RoutePath.AdminConfirmDeleteUser} component={ConfirmDeleteUser} />
              <Route path="*" exact component={NotFound} />
            </Switch>
          </main>
        </>
      )
  )
};

export default Routing;
