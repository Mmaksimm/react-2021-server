import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

const AdminRoute = ({ component: Component, ...rest }) => {
  const {
    isAuthorized,
    user: { isAdmin }
  } = useSelector(({ profile }) => profile);
  return (
    <Route
      {...rest}
      render={props => ((isAuthorized && isAdmin)
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
    />
  )
};

AdminRoute.propTypes = {
  component: PropTypes.any.isRequired, // eslint-disable-line
  location: PropTypes.any // eslint-disable-line
};

AdminRoute.defaultProps = {
  location: undefined
};

export default AdminRoute;
