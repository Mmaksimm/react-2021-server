const ApiPath = {
  Chat: '/api/messages',
  Auth: '/api/auth/',
  Users: '/api/users'
}

export default ApiPath;
