const RoutePath = {
  Chat: '/',
  ChatEdit: '/edit',
  ChatConfirmDelete: '/confirm',
  AdminListUsers: '/users',
  Login: '/login',
  AdminEditUser: '/user-edit',
  AdminConfirmDeleteUser: '/user-confirm-delete'
}

export default RoutePath;
