import { createAsyncThunk, createAction } from '@reduxjs/toolkit';

import * as ActionType from './chatActionTypes';

export const setMessages = createAsyncThunk(ActionType.SET_ALL_MESSAGES, async (url, { extra }) => ({
  messages: await extra.messageService.getAllMessages()
}));

export const addNewMessage = createAsyncThunk(ActionType.ADD_MESSAGE, async (text, { extra }) => ({
  message: await extra.messageService.addMessage({ text })
}))

export const likeToggle = createAsyncThunk(ActionType.UPDATE_MESSAGE, async ({ likeCount, messageId }, { extra }) => {
  const { diffLike } = await extra.messageService.likeMessage(messageId)
  return {
    messageId,
    update: {
      likeCount: (+likeCount + diffLike)
    }
  }
})

export const deleteMessage = createAsyncThunk(ActionType.DELETE_MESSAGE, async (messageId, { extra }) => {
  await extra.messageService.deleteMessage(messageId);
  return;
});

export const editMessage = createAction(ActionType.SET_EDIT_MESSAGE);

export const confirmDeleteMessage = createAction(ActionType.CONFIRM_DELETE_MESSAGE);

export const cancel = createAction(ActionType.CANCEL);

export const updateMessage = createAsyncThunk(ActionType.UPDATE_MESSAGE, async ({ text, messageId }, { extra }) => {
  const { updatedAt } = await extra.messageService.updateMessage({ messageId, text })
  return {
    messageId,
    update: {
      text,
      updatedAt
    }
  }
})

