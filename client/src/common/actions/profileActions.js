import { createAsyncThunk, createAction } from '@reduxjs/toolkit';
import { SET_USER } from './profileActionTypes';
import RoutePath from '../constants/enums/RoutePath';
import StoreName from '../constants/StoreName';

export const loadCurrentUser = createAsyncThunk(SET_USER, async (request, { extra }) => ({
  user: await extra.authService.getCurrentUser()
}));

export const login = createAsyncThunk(SET_USER, async (request, { extra }) => {
  const { user, token } = await extra.authService.login(request);
  localStorage.setItem(StoreName, token);
  return { user }
});

export const logout = createAction(SET_USER, () => {
  localStorage.setItem(StoreName, '');
  window.location.replace(RoutePath.Login);
  return { user: null }
});
