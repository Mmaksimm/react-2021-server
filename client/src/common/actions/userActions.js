import { createAsyncThunk, createAction } from '@reduxjs/toolkit';

import * as ActionType from './userActionTypes';

export const getUsers = createAsyncThunk(ActionType.SET_ALL_USERS, async (url, { extra }) => {
  const users = await extra.userService.getAllUsers();
  return { users };
});

export const addNewUser = createAsyncThunk(ActionType.ADD_USER, async (user, { extra }) => ({
  user: await extra.userService.addUser(user)
}));

export const deleteUser = createAsyncThunk(ActionType.DELETE_USER, async (userId, { extra }) => {
  console.log(userId);
  await extra.userService.deleteUser(userId);
  return;
});

export const editUserAction = createAction(ActionType.SET_EDIT_USER);

export const confirmDeleteUser = createAction(ActionType.CONFIRM_DELETE_USER);

export const cancel = createAction(ActionType.CANCEL);

export const updateUser = createAsyncThunk(ActionType.UPDATE_USER, async ({ userId, ...user }, { extra }) => {
  const { updatedAt } = await extra.userService.updateUser({ userId, user })
  return {
    userId,
    update: {
      ...user,
      updatedAt
    }
  }
})

