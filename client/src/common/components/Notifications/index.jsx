import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({
  user,
  applyPost,
  refreshPost,
  removePost,
  applyComment,
  refreshComment,
  removeComment,
  refreshAllPost
}) => {
  const { REACT_APP_SOCKET_SERVER: address } = process.env;
  const [socket] = useState(io(address));

  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit('createRoom', id);

    socket.on('like-post', ({ postId, message }) => {
      refreshPost(postId);
      NotificationManager.info(message);
    });

    socket.on('new_post', ({ userId, postId, username }) => {
      if (userId !== id) {
        applyPost(postId);
        NotificationManager.info(`${username} created a post.`);
      }
    });

    socket.on('update_post', ({ userId, postId, username = false }) => {
      if (userId !== id) {
        refreshPost(postId);
        if (username) NotificationManager.info(`${username} updated the post`);
      }
    });

    socket.on('delete_post', ({ userId, postId, username }) => {
      if (userId !== id) {
        removePost(postId);
        NotificationManager.info(`${username} deleted the post`);
      }
    });

    socket.on('new_comment', ({ userId, postId, commentId }) => {
      if (userId !== id) {
        applyComment({ postId, commentId });
      }
    });

    socket.on('update_comment', ({ userId, postId, commentId }) => {
      if (userId !== id) {
        refreshComment({ postId, commentId });
      }
    });

    socket.on('delete_comment', ({ userId, postId, commentId }) => {
      if (userId !== id) {
        removeComment({ postId, commentId });
      }
    });

    socket.on('update_user', ({ userId, username, status }) => {
      if (userId !== id) refreshAllPost({ userId, username, status });
    });

    socket.on('like-comment', ({ postId, commentId, userId }) => {
      if (user.id === userId) return;
      refreshComment({ postId, commentId });
    });

    return () => {
      socket.close();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired,
  refreshPost: PropTypes.func.isRequired,
  removePost: PropTypes.func.isRequired,
  applyComment: PropTypes.func.isRequired,
  refreshComment: PropTypes.func.isRequired,
  removeComment: PropTypes.func.isRequired,
  refreshAllPost: PropTypes.func.isRequired
};

export default Notifications;
