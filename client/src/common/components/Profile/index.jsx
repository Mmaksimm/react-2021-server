import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getUserImgLink } from '../../../helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button, Container } from 'semantic-ui-react';
import { logout } from '../../actions/profileActions';
import RoutePath from '../../constants/enums/RoutePath';

import styles from './styles.module.scss';

const Profile = ({ user }) => {
  const {
    login,
    avatar,
    isAdmin
  } = user
  const dispatch = useDispatch();
  const history = useHistory();
  const { pathname } = useLocation();

  const handlerLogOut = () => {
    dispatch(logout());
  }

  const handlerShowUsers = async () => {
    history.push(RoutePath.AdminListUsers);
  }

  const handlerShowChat = async () => {
    history.push(RoutePath.Chat);
  }

  return (
    <Container className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          <HeaderUI className={styles.profile}>
            <Image circular src={getUserImgLink(avatar)} />
            {login}
          </HeaderUI>
        </Grid.Column>
        <Grid.Column textAlign="right">
          {(pathname === RoutePath.AdminListUsers) &&
            <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={handlerShowChat}>
              <Icon name="paper plane" size="large" />
            </Button>
          }
          {(isAdmin && (pathname === RoutePath.Chat)) &&
            <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={handlerShowUsers}>
              <Icon name="users" size="large" />
            </Button>
          }
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={handlerLogOut}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </Container>
  )
};

Profile.propTypes = {
  user: PropTypes.objectOf(
    PropTypes.oneOfType(
      [
        PropTypes.string,
        PropTypes.bool
      ]
    )
  )
};

export default Profile;
